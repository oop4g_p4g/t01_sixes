////////////////////////////////////////////////////////////////////////
// Simple C++ OO program to simulate a simple Dice Game (to be modified) 
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>	
#include <iostream>	
using namespace std; 

//--------Score class
class Score {
public:
	void initialise();
	int getAmount() const;
	void updateAmount(int);
private:
	int amount_;
};
void Score::initialise() {
	amount_ = 0;
}
int Score::getAmount() const {
	return amount_;
}
//increment when value>0, decrement otherwise
void Score::updateAmount(int value) {
	amount_ += value;
}
//--------end of Score class

//---------------------------------------------------------------------------
int main()
{
	void seed();
	int getRandomValue(int);
	int readInNumberOfThrows();

	//seed random number generator
	seed();

	//create and initialise a Score variable
	Score score;
	score.initialise();

	//get number of 'rolls' from user
	const int numberOfGoes(readInNumberOfThrows());

	//show original score
	cout << "\nThe original score is: ";
	cout << score.getAmount() << endl;

	//'roll' dice and update score accordingly 
	for (int i(1); i <= numberOfGoes; i++)
	{
		const int dice = getRandomValue(6);
		cout << endl << "In try #" << i << "\tdice value was: " << dice;
		if (dice == 6)
			score.updateAmount(1);
	}

	//show final score
	cout << "\n\nThe final score is: ";
	cout << score.getAmount() << endl << endl;

	system("pause");	//hold screen open
	return 0;
}

//ask the user for the number of dice throws
int readInNumberOfThrows() {
	int num;
	cout << "\nHow many go do you want? ";
	cin >> num;
	return num;
}

//seed the random number generator from current system time
//so that the numbers will be different every time
void seed() {
	srand(static_cast<unsigned>(time(0)));
}
//produce a random number in range [1..max]
int getRandomValue(int max) {
	return (rand() % max) + 1;
}
